import random
from glob import glob
from PIL import Image
from collections import Counter


SPACING = 32
TILES_PER_ROW = 26
TILES_PER_COL = 18
TILE_WIDTH = 512
TILE_HEIGHT = 512
OUTPUT_WIDTH = 60  # Inches
OUTPUT_HEIGHT = 36  # Inches
DPI = 150

reusecounter = Counter()


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

def maskfunc(i):
    if i > 0:
        return i/8
    else:
        return 0


def main(args):
    #imgwidth = (TILES_PER_ROW * TILE_WIDTH) + (TILES_PER_ROW + 1) * SPACING
    imgheight = ((TILES_PER_COL -1) * TILE_HEIGHT) + (TILES_PER_COL + 1) * SPACING
    imgwidth = int(OUTPUT_WIDTH / OUTPUT_HEIGHT * imgheight)
    TILES_PER_ROW = imgwidth // (TILE_WIDTH)
    images = glob('tiles/*.png')
    secure_random = random.SystemRandom()

    img = Image.new('RGBA', [imgwidth, imgheight], (0xff, 0xFF, 0xFF, 255))

    for y_idx in range(TILES_PER_COL):
        for x_idx in range(TILES_PER_ROW):
            attempts = 0
            x = SPACING + x_idx * TILE_WIDTH + x_idx*SPACING
            y = SPACING + y_idx*TILE_HEIGHT + y_idx*SPACING
            if x_idx % 2 != 0:
                y -= (TILE_HEIGHT // 2)
            choice = secure_random.choice(images)
            while reusecounter[choice] > 2 and attempts < 10:
                choice = secure_random.choice(images)
                attempts += 1
            reusecounter[choice] += 1
            tile = Image.open(choice)
            tile = tile.convert('RGBA')
            alpha = tile.getchannel('A')
            alpha = alpha.point(maskfunc)
            img.paste(tile, (x, y), alpha)
            x += tile.width
            x += SPACING

        x = SPACING

    logo = Image.open('xil.png')
    offsetx = (img.width - logo.width) // 2
    offsety = (img.height - logo.height) // 2
    logoalpha = logo.convert('RGBA').split()[-1]
    img.paste(logo, (offsetx, offsety), logoalpha)
    img = img.resize((OUTPUT_WIDTH*DPI, OUTPUT_HEIGHT*DPI), resample=Image.LANCZOS)
    img.save('flag.png')


if __name__ == '__main__':
    main(None)
